/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_CONTAINERS_EQUALITYUTILITY_HPP
#define OCL_GUARD_CONTAINERS_EQUALITYUTILITY_HPP

#include <cstring>
#include <cwchar>

namespace ocl
{

template<typename Type>
struct EqualityUtility
{
    typedef Type type;

    static bool IsEqual(type const& value1, type const& value2)
    {
        return value1 == value2;
    }
};

template<>
struct EqualityUtility<char const*>
{
    typedef char const* type;

    static bool IsEqual(type value1, type value2) noexcept
    {
        return value1 != nullptr
            ? ((value2 != nullptr) ? value1 == value2 || ::strcmp(value1, value2) == 0 : false)
            : value2 == nullptr;
    }
};

template<>
struct EqualityUtility<char*>
{
    typedef char* type;

    static bool IsEqual(type value1, type value2) noexcept
    {
        return value1 != nullptr
            ? ((value2 != nullptr) ? value1 == value2 || ::strcmp(value1, value2) == 0 : false)
            : value2 == nullptr;
    }
};

template<>
struct EqualityUtility<wchar_t const*>
{
    typedef wchar_t const* type;

    static bool IsEqual(type value1, type value2) noexcept
    {
        return value1 != nullptr
            ? ((value2 != nullptr) ? value1 == value2 || ::wcscmp(value1, value2) == 0 : false)
            : value2 == nullptr;
    }
};

template<>
struct EqualityUtility<wchar_t*>
{
    typedef wchar_t* type;

    static bool IsEqual(type value1, type value2) noexcept
    {
        return value1 != nullptr
            ? ((value2 != nullptr) ? value1 == value2 || ::wcscmp(value1, value2) == 0 : false)
            : value2 == nullptr;
    }
};

template<>
struct EqualityUtility<char16_t const*>
{
    typedef char16_t const* type;

    static bool IsEqual(type value1, type value2) noexcept
    {
        return (value1 != nullptr)
            ? ((value2 != nullptr) ? value1 == value2 || UnsafeCompare(value1, value2) : false)
            : value2 == nullptr;
    }

private:
    static bool UnsafeCompare(type value1, type value2) noexcept
    {
        bool is_equal = value1 == value2;
        if (!is_equal)
            while (*value1 == *value2)
            {
                if (*value1 == 0)
                {
                    is_equal = *value2 == 0;
                    break;
                }
                else if (*value2 == 0)
                {
                    is_equal = false;
                    break;
                }
                ++value1;
                ++value2;
            }
        return is_equal;
    }
};

template<>
struct EqualityUtility<char16_t*>
{
    typedef char16_t* type;

    static bool IsEqual(type value1, type value2) noexcept
    {
        return (value1 != nullptr)
            ? ((value2 != nullptr) ? value1 == value2 || UnsafeCompare(value1, value2) : false)
            : value2 == nullptr;
    }

private:
    static bool UnsafeCompare(type value1, type value2) noexcept
    {
        bool is_equal = value1 == value2;
        if (!is_equal)
            while (*value1 == *value2)
            {
                if (*value1 == 0)
                {
                    is_equal = *value2 == 0;
                    break;
                }
                else if (*value2 == 0)
                {
                    is_equal = false;
                    break;
                }
                ++value1;
                ++value2;
            }
        return is_equal;
    }
};

template<>
struct EqualityUtility<char32_t const*>
{
    typedef char32_t const* type;

    static bool IsEqual(type value1, type value2) noexcept
    {
        return (value1 != nullptr)
            ? ((value2 != nullptr) ? value1 == value2 || UnsafeCompare(value1, value2) : false)
            : value2 == nullptr;
    }

private:
    static bool UnsafeCompare(type value1, type value2) noexcept
    {
        bool is_equal = value1 == value2;
        if (!is_equal)
            while (*value1 == *value2)
            {
                if (*value1 == 0)
                {
                    is_equal = *value2 == 0;
                    break;
                }
                else if (*value2 == 0)
                {
                    is_equal = false;
                    break;
                }
                ++value1;
                ++value2;
            }
        return is_equal;
    }
};

template<>
struct EqualityUtility<char32_t*>
{
    typedef char32_t* type;

    static bool IsEqual(type value1, type value2) noexcept
    {
        return (value1 != nullptr)
            ? ((value2 != nullptr) ? value1 == value2 || UnsafeCompare(value1, value2) : false)
            : value2 == nullptr;
    }

private:
    static bool UnsafeCompare(type value1, type value2) noexcept
    {
        bool is_equal = value1 == value2;
        if (!is_equal)
            while (*value1 == *value2)
            {
                if (*value1 == 0)
                {
                    is_equal = *value2 == 0;
                    break;
                }
                else if (*value2 == 0)
                {
                    is_equal = false;
                    break;
                }
                ++value1;
                ++value2;
            }
        return is_equal;
    }
};

} // namespace ocl

#endif // OCL_GUARD_CONTAINERS_EQUALITYUTILITY_HPP
