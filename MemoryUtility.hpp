/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_CONTAINERS_MEMORYUTILITY_HPP
#define OCL_GUARD_CONTAINERS_MEMORYUTILITY_HPP

#include <cstddef>
#include <cstdint>
#include "PrimitiveMemoryUtility.hpp"
#include "IsPrimitive.hpp"

namespace ocl
{

template<typename Type, typename SizeType = std::size_t, bool const force_primitive = false>
struct MemoryUtility
{
    typedef Type type;
    typedef SizeType size_type;
    typedef PrimitiveMemoryUtility<type, size_type> primitive_memory_util_type;

    static bool const is_primitive = IsPrimitive<Type>::is_primitive || force_primitive;

    static type* Allocate()
    {
        return is_primitive ? primitive_memory_util_type::Allocate() : new type();
    }

    static type* Allocate(type const& other)
    {
        return is_primitive ? primitive_memory_util_type::Allocate(other) : new type(other);
    }

    static type* Allocate(type&& other)
    {
        return is_primitive
            ? primitive_memory_util_type::Allocate(static_cast<type&&>(other))
            : new type(static_cast<type&&>(other));
    }

    static void Free(type* ptr)
    {
        is_primitive ? primitive_memory_util_type::Free(ptr) : delete ptr;
    }

    static type* AllocateArray(size_type size)
    {
        return is_primitive ? primitive_memory_util_type::AllocateArray(size) : new type[size];
    }

    static void FreeArray(type* ptr)
    {
        delete [] ptr;
    }
};

} // namespace ocl

#endif // OCL_GUARD_CONTAINERS_MEMORYUTILITY_HPP
