/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_CONTAINERS_PRIMITIVEMEMORYUTILITY_HPP
#define OCL_GUARD_CONTAINERS_PRIMITIVEMEMORYUTILITY_HPP

#include <cstddef>

namespace ocl
{

// Allocator for primitive and pointer types using malloc/free.
template<typename Type, typename SizeType = std::size_t>
struct PrimitiveMemoryUtility
{
    typedef Type type;
    typedef SizeType size_type;

    static type* Allocate()
    {
        return static_cast<type*>(malloc(sizeof(type)));
    }

    static type* Allocate(type const& other)
    {
        type* ptr = static_cast<type*>(malloc(sizeof(type)));
        if (ptr != nullptr)
            ::memcpy(ptr, &other, sizeof(type));
        return ptr;
    }

    static type* Allocate(type&& other)
    {
        type* ptr = static_cast<type*>(malloc(sizeof(type)));
        if (ptr != nullptr)
            ::memcpy(ptr, &other, sizeof(type));
        return ptr;
    }

    static void Free(type* ptr)
    {
        free(ptr);
    }

    static type* AllocateArray(size_type size)
    {
        return static_cast<type*>(malloc(sizeof(type) * size));
    }

    static void FreeArray(type* ptr)
    {
        free(ptr);
    }
};

} // namespace ocl

#endif // OCL_GUARD_CONTAINERS_PRIMITIVEMEMORYUTILITY_HPP
