/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_CONTAINERS_ISPRIMITIVE_HPP
#define OCL_GUARD_CONTAINERS_ISPRIMITIVE_HPP

#include <cstddef>
#include <cstdint>

namespace ocl
{

template<typename Type>
struct IsPrimitive
{
    static bool const is_primitive = false;
};

template<typename Type>
struct IsPrimitive<Type*>
{
    static bool const is_primitive = true;
};

template<typename Type>
struct IsPrimitive<Type const*>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<bool>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<char>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<wchar_t>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<char16_t>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<char32_t>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<signed char>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<unsigned char>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<signed short>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<unsigned short>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<signed int>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<unsigned int>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<signed long int>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<unsigned long int>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<signed long long int>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<unsigned long long int>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<float>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<double>
{
    static bool const is_primitive = true;
};

template<>
struct IsPrimitive<long double>
{
    static bool const is_primitive = true;
};

} // namespace ocl

#endif // OCL_GUARD_CONTAINERS_ISPRIMITIVE_HPP
