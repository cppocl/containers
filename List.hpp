/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_CONTAINERS_LIST_HPP
#define OCL_GUARD_CONTAINERS_LIST_HPP

#include "NextNode.hpp"
#include "PrevNextNode.hpp"
#include "IsPrimitive.hpp"
#include "MemoryUtility.hpp"
#include <cstddef>

namespace ocl
{

template< typename ValueType,
          typename SizeType = std::size_t,
          typename NodeType = PrevNextNode<ValueType, SizeType>,
          typename MemoryUtilityType = MemoryUtility<NodeType, SizeType> >
class List
{
public:
    typedef ValueType value_type;
    typedef SizeType  size_type;
    typedef NodeType  node_type;
    typedef MemoryUtilityType memory_utility_type;

public:
    List()
        : m_head(nullptr)
        , m_tail(nullptr)
    {
    }

    /// Count number of nodes in list and return total.
    size_type GetCount() const noexcept
    {
        return m_head != nullptr ? m_head->GetForwardCount() : 0;
    }

    /// Get the head node.
    node_type* GetHeadNode() noexcept
    {
        return m_head;
    }

    /// Get the head node as a const pointer.
    node_type const* GetHeadNode() const noexcept
    {
        return m_head;
    }

    /// Get the tail node.
    node_type* GetTailNode() noexcept
    {
        return m_tail;
    }

    /// Get the tail node as a const pointer.
    node_type const* GetTailNode() const noexcept
    {
        return m_tail;
    }

    node_type* FindNode(value_type const& value)
    {
        // NOTE: FindNextValue should be faster,
        //       but the optimizer is produce faster code with FindValue.
        return m_head != nullptr ? m_head->FindValue(value) : nullptr;
    }

    node_type* const FindNode(value_type const& value) const
    {
        // NOTE: FindNextValue should be faster,
        //       but the optimizer is produce faster code with FindValue.
        return m_head != nullptr ? m_head->FindValue(value) : nullptr;
    }

    bool Exists(value_type const& value) const
    {
        return FindNode(value) != nullptr;
    }

    void AddHead(value_type const& value)
    {
        node_type* node = UnsafeAlloc(value);
        if (m_head == nullptr)
            m_head = m_tail = node;
        else if (node != nullptr)
        {
            m_head->UnsafeAddHead(node);
            m_head = node;
        }
    }

    void AddHead(value_type&& value)
    {
        node_type* node = UnsafeAlloc(static_cast<value_type&&>(value));
        if (m_head == nullptr)
            m_head = m_tail = node;
        else if (node != nullptr)
        {
            m_head->UnsafeAddHead(node);
            m_head = node;
        }
    }

    void AddTail(value_type const& value)
    {
        node_type* node = UnsafeAlloc(value);
        if (m_tail == nullptr)
            m_head = m_tail = node;
        else if (node != nullptr)
        {
            m_tail->UnsafeAddTail(node);
            m_tail = node;
        }
    }

    void AddTail(value_type&& value)
    {
        node_type* node = UnsafeAlloc(static_cast<value_type&&>(value));
        if (m_tail == nullptr)
            m_head = m_tail = node;
        else if (node != nullptr)
        {
            m_tail->UnsafeAddTail(node);
            m_tail = node;
        }
    }

    void InsertBefore(node_type* pos, value_type const& value)
    {
        if (pos != nullptr)
        {
            node_type* node = UnsafeAlloc(value);
            pos->InsertBefore(node);
        }
    }

    void InsertBefore(node_type* pos, value_type&& value)
    {
        if (pos != nullptr)
        {
            node_type* node = UnsafeAlloc(static_cast<value_type&&>(value));
            pos->InsertBefore(node);
        }
    }

    void InsertAfter(node_type* pos, value_type const& value)
    {
        if (pos != nullptr)
        {
            node_type* node = UnsafeAlloc(value);
            pos->InsertAfter(node);
        }
    }

    void InsertAfter(node_type* pos, value_type&& value)
    {
        if (pos != nullptr)
        {
            node_type* node = UnsafeAlloc(static_cast<value_type&&>(value));
            pos->InsertAfter(node);
        }
    }

    void Remove(node_type* node)
    {
        if (node != nullptr)
        {
            if (node == m_head)
            {
                if (m_head == m_tail)
                {
                    m_head = m_tail = nullptr;
                    node->Remove();
                }
                else
                {
                    node_type* next = m_head->GetNext();
                    node->Remove();
                    m_head = next;
                }
            }
            else if (node == m_tail)
            {
                node_type* prev = m_tail->GetPrev();
                node->Remove();
                m_tail = prev;
            }
            else
                node->Remove();
            Free(node);
        }
    }

private:
    static node_type* UnsafeAlloc(value_type const& value)
    {
        return memory_utility_type::Allocate(node_type(value));
    }

    static node_type* UnsafeAlloc(value_type&& value)
    {
        return memory_utility_type::Allocate(node_type(static_cast<value_type&&>(value)));
    }

    static void Free(node_type* node)
    {
        memory_utility_type::Free(node);
    }

private:
    node_type* m_head;
    node_type* m_tail;
};

} // namespace ocl

#endif // OCL_GUARD_CONTAINERS_LIST_HPP
