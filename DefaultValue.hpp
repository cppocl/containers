/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_CONTAINERS_DEFAULTVALUE_HPP
#define OCL_GUARD_CONTAINERS_DEFAULTVALUE_HPP

#include <cstddef>
#include <cstdint>

template<typename Type>
struct DefaultValue
{
    static Type constexpr Value() { return Type(); }
};

template<typename Type>
struct DefaultValue<Type*>
{
    static constexpr Type* Value() { return nullptr; }
};

template<typename Type>
struct DefaultValue<Type const*>
{
    static constexpr Type const* Value() { return nullptr; }
};

template<>
struct DefaultValue<bool>
{
    static constexpr bool Value() { return false; }
};

template<>
struct DefaultValue<char>
{
    static constexpr char Value() { return '\0'; }
};

template<>
struct DefaultValue<wchar_t>
{
    static constexpr wchar_t Value() { return L'\0'; }
};

template<>
struct DefaultValue<char16_t>
{
    static constexpr char16_t Value() { return u'\0'; }
};

template<>
struct DefaultValue<char32_t>
{
    static constexpr char32_t Value() { return U'\0'; }
};

template<>
struct DefaultValue<signed char>
{
    static constexpr signed char Value() { return 0; }
};

template<>
struct DefaultValue<unsigned char>
{
    static constexpr unsigned char Value() { return 0; }
};

template<>
struct DefaultValue<signed short>
{
    static constexpr signed short Value() { return 0; }
};

template<>
struct DefaultValue<unsigned short>
{
    static constexpr unsigned short Value() { return 0; }
};

template<>
struct DefaultValue<signed int>
{
    static constexpr signed int Value() { return 0; }
};

template<>
struct DefaultValue<unsigned int>
{
    static constexpr unsigned int Value() { return 0; }
};

template<>
struct DefaultValue<signed long int>
{
    static constexpr signed long int Value() { return 0; }
};

template<>
struct DefaultValue<unsigned long int>
{
    static constexpr unsigned long int Value() { return 0; }
};

template<>
struct DefaultValue<signed long long int>
{
    static constexpr signed long long int Value() { return 0; }
};

template<>
struct DefaultValue<unsigned long long int>
{
    static constexpr unsigned long long int Value() { return 0; }
};

template<>
struct DefaultValue<float>
{
    static constexpr float Value() { return 0.0f; }
};

template<>
struct DefaultValue<double>
{
    static constexpr double Value() { return 0.0; }
};

template<>
struct DefaultValue<long double>
{
    static constexpr long double Value() { return 0.0; }
};

#endif // OCL_GUARD_CONTAINERS_DEFAULTVALUE_HPP
