/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_CONTAINERS_MOVEUTILITY_HPP
#define OCL_GUARD_CONTAINERS_MOVEUTILITY_HPP

namespace ocl
{

template<typename Type>
struct MoveUtility
{
    typedef Type type;

    static void Move(Type& destination, Type& source) noexcept
    {
        destination = static_cast<Type&&>(source);
    }

    /// Complete move operation, when move is used within constructor.
    static void PostMove(Type&) noexcept
    {
        // Nothing to do for non-pointer types.
    }
};

template<typename Type>
struct MoveUtility<Type*>
{
    typedef Type* type;

    static void Move(type& destination, type& source) noexcept
    {
        destination = source;
        source = nullptr;
    }

    /// Complete move operation, when move is used within constructor.
    static void PostMove(type& value) noexcept
    {
        value = nullptr;
    }
};

} // namespace ocl

#endif // OCL_GUARD_CONTAINERS_MOVEUTILITY_HPP
