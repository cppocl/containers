/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_CONTAINERS_XORSWAPUTILITY_HPP
#define OCL_GUARD_CONTAINERS_XORSWAPUTILITY_HPP

#include <cstddef>
#include <cstdint>

namespace ocl
{

template<typename Type>
struct XorSwapUtility
{
    typedef Type type;

    static void XorSwap(type& a, type& b) noexcept
    {
        a ^= b;
        b ^= a;
        a ^= b;
    }
};

template<typename Type>
struct XorSwapUtility<Type*>
{
    typedef Type* type;

    static void XorSwap(type& a, type& b) noexcept
    {
        static_assert(sizeof(type) == sizeof(std::uintptr_t), "Pointer and uintptr_t don't match size!");

        a = reinterpret_cast<type>(reinterpret_cast<std::uintptr_t>(a) ^ reinterpret_cast<std::uintptr_t>(b));
        b = reinterpret_cast<type>(reinterpret_cast<std::uintptr_t>(b) ^ reinterpret_cast<std::uintptr_t>(a));
        a = reinterpret_cast<type>(reinterpret_cast<std::uintptr_t>(a) ^ reinterpret_cast<std::uintptr_t>(b));
    }
};

} // namespace ocl

#endif // OCL_GUARD_CONTAINERS_XORSWAPUTILITY_HPP
