/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_CONTAINERS_PREVNEXTNODE_HPP
#define OCL_GUARD_CONTAINERS_PREVNEXTNODE_HPP

#include "MoveUtility.hpp"
#include "IsPrimitive.hpp"
#include "SwapUtility.hpp"
#include "DefaultValue.hpp"
#include "EqualityUtility.hpp"
#include "MemoryUtility.hpp"
#include <cstddef>

namespace ocl
{

/// PrevNextNode for a doubly linked list (previous/next nodes).
template< typename Type,
          typename SizeType = std::size_t,
          typename CompareType = EqualityUtility<Type> >
class PrevNextNode
{
public:

    typedef Type type;
    typedef SizeType size_type;
    typedef CompareType compare_type;
    typedef PrevNextNode<type, size_type, compare_type> node_type;

public:
    PrevNextNode()
        : m_value(DefaultValue<type>::Value())
        , m_prev(nullptr)
        , m_next(nullptr)
    {
    }

    PrevNextNode(type const& value)
        : m_value(value)
        , m_prev(nullptr)
        , m_next(nullptr)
    {
    }

    PrevNextNode(type&& value)
        : m_value(static_cast<type&&>(value))
        , m_prev(nullptr)
        , m_next(nullptr)
    {
        // Set any pointers to null for a value move.
        MoveUtility<type>::PostMove(value);
    }

    PrevNextNode(node_type&& node)
        : m_value(static_cast<type&&>(node.m_value))
        , m_prev(node.m_prev)
        , m_next(node.m_next)
    {
        /// Set any pointers to null for a value move.
        MoveUtility<type>::PostMove(node.m_value);
        node.m_prev = nullptr;
        node.m_next = nullptr;
    }

    PrevNextNode(type const& value, node_type* prev, node_type* next)
        : m_value(value)
        , m_prev(prev)
        , m_next(next)
    {
    }

    PrevNextNode(type&& value, node_type* prev, node_type* next)
        : m_value(static_cast<type&&>(value))
        , m_prev(prev)
        , m_next(next)
    {
        /// Set any pointers to null for a value move.
        MoveUtility<type>::PostMove(value);
    }

    PrevNextNode(node_type&& node, node_type* prev, node_type* next)
        : m_value(static_cast<type&&>(node.m_value))
        , m_prev(prev)
        , m_next(next)
    {
        /// Set any pointers to null for a value move.
        MoveUtility<type>::PostMove(node.m_value);
        node.m_prev = nullptr;
        node.m_next = nullptr;
    }

    /// Don't allow copying or moving of a node, as this could break
    /// the node list.
    node_type& operator=(node_type const&) = delete;
    node_type& operator=(node_type&&) = delete;

    ~PrevNextNode()
    {
    }

    type const& GetValue() const noexcept
    {
        return m_value;
    }

    type& GetValue() noexcept
    {
        return m_value;
    }

    void SetValue(type const& value)
    {
        m_value = value;
    }

    void SetValue(type&& value)
    {
        MoveUtility<type>::Move(m_value, value);
    }

    void MoveValue(type& value)
    {
        MoveUtility<type>::Move(m_value, value);
    }

    size_type GetCount() const noexcept
    {
        size_type count = 1;
        node_type const* node = GetHead();

        while (node->m_next != nullptr)
        {
            node = node->m_next;
            ++count;
        }

        return count;
    }

    /// Get number of nodes from this node to the front.
    size_type GetReverseCount() const noexcept
    {
        size_type count = 1;
        node_type const* node = this;

        while (node->m_prev != nullptr)
        {
            node = node->m_prev;
            ++count;
        }

        return count;
    }

    /// Get number of nodes from this node to the end.
    size_type GetForwardCount() const noexcept
    {
        size_type count = 1;
        node_type const* node = this;

        while (node->m_next != nullptr)
        {
            node = node->m_next;
            ++count;
        }

        return count;
    }

    /// Return the previous node, or null if this is the head node.
    node_type const* GetPrev() const noexcept
    {
        return m_prev;
    }

    /// Return the previous node, or null if this is the head node.
    node_type* GetPrev() noexcept
    {
        return m_prev;
    }

    /// Return the next node, or null if this is the tail node.
    node_type const* GetNext() const noexcept
    {
        return m_next;
    }

    /// Return the next node, or null if this is the tail node.
    node_type* GetNext() noexcept
    {
        return m_next;
    }

    /// Get the head node, which is never null.
    /// The head node could be this node.
    node_type const* GetHead() const noexcept
    {
        node_type const* head = this;
        while (head->m_prev != nullptr)
            head = head->m_prev;
        return head;
    }

    /// Get the head node, which is never null.
    /// The head node could be this node.
    node_type* GetHead() noexcept
    {
        node_type* head = this;
        while (head->m_prev != nullptr)
            head = head->m_prev;
        return head;
    }

    /// Get the tail node, which is never null.
    /// The tail node could be this node.
    node_type* GetTail() noexcept
    {
        node_type* tail = this;
        while (tail->m_next != nullptr)
            tail = tail->m_next;
        return tail;
    }

    /// Get the tail node, which is never null.
    /// The tail node could be this node.
    node_type const* GetTail() const noexcept
    {
        node_type const* tail = this;
        while (tail->m_next != nullptr)
            tail = tail->m_next;
        return tail;
    }

    /// Find a value, starting from the current node, iterating to the head,
    /// then iterating from this node to the tail.
    node_type* FindValue(type const& value)
    {
        node_type* node = FindNextValue(value);
        return (node != nullptr) ? node : FindPrevValue(value);
    }

    /// Find a value, starting from the current node, iterating to the head,
    /// then iterating from this node to the tail.
    node_type const* FindValue(type const& value) const
    {
        node_type const* node = FindNextValue(value);
        return (node != nullptr) ? node : FindPrevValue(value);
    }

    /// Find a value, starting from the current node, iterating to the head.
    node_type* FindPrevValue(type const& value)
    {
        node_type* node = this;
        while (node != nullptr && !compare_type::IsEqual(node->GetValue(), value))
            node = node->m_prev;
        return node;
    }

    /// Find a value, starting from the current node, iterating to the head.
    node_type const* FindPrevValue(type const& value) const
    {
        node_type const* node = this;
        while (node != nullptr && !compare_type::IsEqual(node->GetValue(), value))
            node = node->m_prev;
        return node;
    }

    /// Find a value, starting from the current node, iterating to the tail.
    node_type* FindNextValue(type const& value)
    {
        node_type* node = this;
        while (node != nullptr && !compare_type::IsEqual(node->GetValue(), value))
            node = node->m_next;
        return node;
    }

    /// Find a value, starting from the current node, iterating to the tail.
    node_type const* FindNextValue(type const& value) const
    {
        node_type const* node = this;
        while (node != nullptr && !compare_type::IsEqual(node->GetValue(), value))
            node = node->m_next;
        return node;
    }

    /// Add node to the front of the node list.
    void AddHead(node_type* node) noexcept
    {
        if (node != nullptr)
            UnsafeAddHead(GetHead(), node);
    }

    /// Add node to the end of the node list.
    void AddTail(node_type* node) noexcept
    {
        if (node != nullptr)
            UnsafeAddTail(GetTail(), node);
    }

    /// Node will be inserted before this node, or at front if this node
    /// is the head node.
    void InsertBefore(node_type* node) noexcept
    {
        if (node != nullptr)
        {
            if (m_prev != nullptr)
                UnsafeInsertBetween(m_prev, this, node);
            else
                AddHead(node);
        }
    }

    /// Node will be inserted after this node, or at end if this node
    /// is the tail node.
    void InsertAfter(node_type* node) noexcept
    {
        if (node != nullptr)
        {
            if (m_next != nullptr)
                UnsafeInsertBetween(this, m_next, node);
            else
                AddTail(node);
        }
    }

    /// Remove the node from the other nodes.
    void Remove() noexcept
    {
        UnsafeRemove(this);
    }

    void Swap(node_type* node) noexcept
    {
        UnsafeSwap(this, node);
    }

    // unsafe...
    /// Add node to the front of the node list.
    void UnsafeAddHead(node_type* node) noexcept
    {
        UnsafeAddHead(GetHead(), node);
    }

    /// Add node to the end of the node list.
    void UnsafeAddTail(node_type* node) noexcept
    {
        UnsafeAddTail(GetTail(), node);
    }

    /// Node will be inserted before this node, or at front if this node
    /// is the head node.
    void UnsafeInsertBefore(node_type* node) noexcept
    {
        if (m_prev != nullptr)
            UnsafeInsertBetween(m_prev, this, node);
        else
            AddHead(node);
   }

    /// Node will be inserted after this node, or at end if this node
    /// is the tail node.
    void UnsafeInsertAfter(node_type* node) noexcept
    {
        if (m_next != nullptr)
            UnsafeInsertBetween(this, m_next, node);
        else
            AddTail(node);
    }


public:
    /// PrevNextNode does not store a head pointer, so return false.
    static constexpr bool SupportHead() noexcept
    {
        return false;
    }

    /// PrevNextNode does not store a tail pointer, so return false.
    static constexpr bool SupportTail() noexcept
    {
        return false;
    }

    /// PrevNextNode has a previous pointer, so return true.
    static constexpr bool SupportPrevious() noexcept
    {
        return true;
    }

    /// PrevNextNode has a next pointer, so return true.
    static constexpr bool SupportNext() noexcept
    {
        return true;
    }

    /// Insert node between first and second nodes, updating the prev and next nodes.
    /// All pointers must be valid, as there is no null pointer checking.
    static void UnsafeInsertBetween(node_type* first, node_type* second, node_type* node)
    {
        first->m_next = node;
        second->m_prev = node;
        node->m_prev = first;
        node->m_next = second;
    }

    /// Replace current head with node, updating existing head to point to node.
    static void UnsafeAddHead(node_type* head, node_type* node)
    {
        head->m_prev = node;
        node->m_prev = nullptr;
        node->m_next = head;
    }

    /// Replace current tail with node, updating existing tail to point to node.
    static void UnsafeAddTail(node_type* tail, node_type* node)
    {
        tail->m_next = node;
        node->m_next = nullptr;
        node->m_prev = tail;
    }

    /// Remove the node from any related nodes, by clearing the previous and next node pointers.
    static void UnsafeRemove(node_type* node)
    {
        if (node->m_prev != nullptr)
            node->m_prev->m_next = node->m_next;
        if (node->m_next != nullptr)
            node->m_next->m_prev = node->m_prev;
        node->m_next = node->m_prev = nullptr;
    }

    static void UnsafeSwap(node_type* a, node_type* b)
    {
        // Most likely it will be quicker to swap the value for primitive and pointer types.
        // Complex types could be slow to swap, so swapping prev and next pointers is chosen.
        IsPrimitive<type>::is_primitive ? UnsafeSwapValue(a->m_value, b->m_value) :
                                          UnsafeSwapPrevNext(a, b);
    }

    template<typename MemoryUtilityType = MemoryUtility<node_type, size_type, IsPrimitive<type>::is_primitive> >
    static node_type* Allocate(type const& value)
    {
        return MemoryUtilityType::Allocate(value);
    }

    template<typename MemoryUtilityType = MemoryUtility<node_type, size_type, IsPrimitive<type>::is_primitive> >
    static void Free(node_type* node)
    {
        MemoryUtilityType::Free(node);
    }

    /// Free memory for head node and all other nodes to the end.
    template<typename MemoryUtilityType = MemoryUtility<node_type, size_type, IsPrimitive<type>::is_primitive> >
    static void FreeNodes(node_type*& head_node)
    {
        node_type* head = head_node;
        head_node = nullptr;
        while (head != nullptr)
        {
            node_type* prev = head;
            head = head->m_next;
            MemoryUtilityType::Free(prev);
        }
    }

private:
    static void UnsafeSwapValue(type& a, type& b)
    {
        SwapUtility<type>::Swap(a, b);
    }

    /// Set the previous and next nodes for a node,
    /// ensuring previous and next don't point to the node being set.
    /// NOTE: a and b cannot be null.
    /*
        Test data:
            a.value = 1
            a.prev = null
            a.next = b
            b.value = 2
            b.prev = a
            b.next = c
            c.value = 3
            c.prev = b
            c.next = null

        After swapping a and b
            a.value = 1
            a.prev = b
            a.next = c
            b.value = 2
            b.prev = null
            b.next = a
    */
    static void UnsafeSwapPrevNext(node_type* a, node_type* b, node_type* b_prev, node_type* b_next)
    {
        if (a != b_prev)
        {
            a->m_prev = b_prev;
            a->m_next = (a != b_next) ? b_next : b;
        }
        else
        {
            a->m_prev = b;
            a->m_next = b_next; // As a points to b_prev, a cannot also point to b_next.
        }
    }

    /// Swap the previous and next pointers for nodes a and b.
    static void UnsafeSwapPrevNext(node_type* a, node_type* b)
    {
        node_type* a_prev = a->m_prev;
        node_type* a_next = a->m_next;
        UnsafeSwapPrevNext(a, b, b->m_prev, b->m_next);
        UnsafeSwapPrevNext(b, a, a_prev, a_next);
    }

private:
    type m_value;
    node_type* m_prev;
    node_type* m_next;
};

} // namespace ocl

#endif // OCL_GUARD_CONTAINERS_PREVNEXTNODE_HPP
    