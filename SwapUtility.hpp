/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_CONTAINERS_SWAPUTILITY_HPP
#define OCL_GUARD_CONTAINERS_SWAPUTILITY_HPP

#include "XorSwapUtility.hpp"
#include <cstddef>
#include <cstdint>

namespace ocl
{

template<typename Type>
struct SwapUtility
{
    typedef Type type;

    static void Swap(Type& a, Type& b)
    {
        Type temp(a);
        a = static_cast<Type&&>(b);
        b = static_cast<Type&&>(temp);
    }
};

template<typename Type>
struct SwapUtility<Type*>
{
    typedef Type* type;

    static void Swap(type& a, type& b) noexcept
    {
        XorSwapUtility<type>::XorSwap(a, b);
    }
};

template<>
struct SwapUtility<char>
{
    typedef char type;

    static void Swap(type& a, type& b) noexcept
    {
        XorSwapUtility<type>::XorSwap(a, b);
    }
};

template<>
struct SwapUtility<wchar_t>
{
    typedef wchar_t type;

    static void Swap(type& a, type& b) noexcept
    {
        XorSwapUtility<type>::XorSwap(a, b);
    }
};

template<>
struct SwapUtility<char16_t>
{
    typedef char16_t type;

    static void Swap(type& a, type& b) noexcept
    {
        XorSwapUtility<type>::XorSwap(a, b);
    }
};

template<>
struct SwapUtility<char32_t>
{
    typedef char32_t type;

    static void Swap(type& a, type& b) noexcept
    {
        XorSwapUtility<type>::XorSwap(a, b);
    }
};

template<>
struct SwapUtility<signed char>
{
    typedef signed char type;

    static void Swap(type& a, type& b) noexcept
    {
        XorSwapUtility<type>::XorSwap(a, b);
    }
};

template<>
struct SwapUtility<unsigned char>
{
    typedef unsigned char type;

    static void Swap(type& a, type& b) noexcept
    {
        XorSwapUtility<type>::XorSwap(a, b);
    }
};

template<>
struct SwapUtility<signed short>
{
    typedef signed short type;

    static void Swap(type& a, type& b) noexcept
    {
        XorSwapUtility<type>::XorSwap(a, b);
    }
};

template<>
struct SwapUtility<unsigned short>
{
    typedef unsigned short type;

    static void Swap(type& a, type& b) noexcept
    {
        XorSwapUtility<type>::XorSwap(a, b);
    }
};

template<>
struct SwapUtility<signed int>
{
    typedef signed int type;

    static void Swap(type& a, type& b) noexcept
    {
        XorSwapUtility<type>::XorSwap(a, b);
    }
};

template<>
struct SwapUtility<unsigned int>
{
    typedef unsigned int type;

    static void Swap(type& a, type& b) noexcept
    {
        XorSwapUtility<type>::XorSwap(a, b);
    }
};

template<>
struct SwapUtility<signed long int>
{
    typedef signed long int type;

    static void Swap(type& a, type& b) noexcept
    {
        XorSwapUtility<type>::XorSwap(a, b);
    }
};

template<>
struct SwapUtility<unsigned long int>
{
    typedef unsigned long int type;

    static void Swap(type& a, type& b) noexcept
    {
        XorSwapUtility<type>::XorSwap(a, b);
    }
};

template<>
struct SwapUtility<signed long long int>
{
    typedef signed long long int type;

    static void Swap(type& a, type& b) noexcept
    {
        XorSwapUtility<type>::XorSwap(a, b);
    }
};

template<>
struct SwapUtility<unsigned long long int>
{
    typedef unsigned long long int type;

    static void Swap(type& a, type& b) noexcept
    {
        XorSwapUtility<type>::XorSwap(a, b);
    }
};

} // namespace ocl

#endif // OCL_GUARD_CONTAINERS_SWAPUTILITY_HPP
