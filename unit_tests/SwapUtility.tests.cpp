/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../SwapUtility.hpp"

TEST_MEMBER_FUNCTION(SwapUtility, XorSwap, int_ref_int_ref)
{
    TEST_OVERRIDE_ARGS("int&, int&");

    struct BigData
    {
        long a, b, c, d;

        bool operator ==(BigData const& other) const
        {
            return memcmp(this, &other, sizeof(other)) == 0;
        }
    };

    {
        typedef int type;
        typedef ocl::SwapUtility<type> swap_utility_type;

        type a = 1;
        type b = 2;
        swap_utility_type::Swap(a, b);
        CHECK_EQUAL(a, 2);
        CHECK_EQUAL(b, 1);

        type* pa = &a;
        type* pb = &b;
        ocl::SwapUtility<type*>::Swap(pa, pb);
        CHECK_EQUAL(pa, &b);
        CHECK_EQUAL(pb, &a);
    }

    {
        typedef BigData type;
        typedef ocl::SwapUtility<type> swap_utility_type;

        type const A = { 1, 2, 3, 4 };
        type const B = { 5, 6, 7, 8 };

        type a = A;
        type b = B;
        swap_utility_type::Swap(a, b);
        CHECK_EQUAL(a, B);
        CHECK_EQUAL(b, A);
    }
}
