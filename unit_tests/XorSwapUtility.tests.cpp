/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../XorSwapUtility.hpp"

TEST_MEMBER_FUNCTION(XorSwapUtility, XorSwap, int_ref_int_ref)
{
    TEST_OVERRIDE_ARGS("int&, int&");

    typedef int type;
    typedef ocl::XorSwapUtility<type> xor_swap_utility_type;

    type a = 1;
    type b = 2;
    xor_swap_utility_type::XorSwap(a, b);
    CHECK_EQUAL(a, 2);
    CHECK_EQUAL(b, 1);

    type* pa = &a;
    type* pb = &b;
    ocl::XorSwapUtility<type*>::XorSwap(pa, pb);
    CHECK_EQUAL(pa, &b);
    CHECK_EQUAL(pb, &a);
}
