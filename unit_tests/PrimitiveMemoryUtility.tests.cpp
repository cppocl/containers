/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../PrimitiveMemoryUtility.hpp"

TEST_MEMBER_FUNCTION(PrimitiveMemoryUtility, Allocate, NA)
{
    typedef char type;
    typedef std::size_t size_type;

    bool exception = false;

    typedef ocl::PrimitiveMemoryUtility<type, size_type> memory_utility_type;

    type* ptr = nullptr;
    try
    {
        ptr = memory_utility_type::Allocate();
        CHECK_NOT_NULL(ptr);
        memory_utility_type::Free(ptr);
    }

    catch (...)
    {
        exception = true;
    }

    CHECK_FALSE(exception);
}

TEST_MEMBER_FUNCTION(PrimitiveMemoryUtility, Allocate, type)
{
    typedef char type;
    typedef std::size_t size_type;

    bool exception = false;

    typedef ocl::PrimitiveMemoryUtility<type, size_type> memory_utility_type;

    type* ptr = nullptr;
    try
    {
        ptr = memory_utility_type::Allocate('a');
        CHECK_NOT_NULL(ptr);
        CHECK_EQUAL(*ptr, 'a');
        memory_utility_type::Free(ptr);
    }

    catch (...)
    {
        exception = true;
    }

    CHECK_FALSE(exception);
}

TEST_MEMBER_FUNCTION(PrimitiveMemoryUtility, AllocateArray, size_t)
{
    typedef char type;
    typedef std::size_t size_type;

    bool exception = false;

    typedef ocl::PrimitiveMemoryUtility<type, size_type> memory_utility_type;

    type* ptr = nullptr;
    try
    {
        ptr = memory_utility_type::AllocateArray(2);
        CHECK_NOT_NULL(ptr);
        memory_utility_type::FreeArray(ptr);
    }

    catch (...)
    {
        exception = true;
    }

    CHECK_FALSE(exception);
}
