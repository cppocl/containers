/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../List.hpp"
#include <list>
#include <cassert>

namespace
{
    int remove_count = 1000000;

    typedef char const* type;
    typedef std::size_t size_type;

    type args[53] =
    {
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", ""
    };

    static ocl::List<char const*, std::size_t>* GetOclList()
    {
        typedef ocl::List<type, size_type> list_type;

        list_type* l = new list_type();
        if (l->GetHeadNode() == nullptr)
        {
            for (size_t pos = 0; args[pos][0] != '\0'; ++pos)
            {
                type arg = args[pos];
                l->AddTail(arg);
            }
        }
        return l;
    }

    static std::list<char const*>* GetStdList()
    {
        typedef std::list<type> list_type;

        list_type* l = new list_type();
        if (l->empty())
        {
            for (size_t pos = 0; args[pos][0] != '\0'; ++pos)
            {
                type arg = args[pos];
                l->push_back(arg);
            }
        }
        return l;
    }
}

TEST_CONST_MEMBER_FUNCTION_TIME(List, AddHead, NA, 0, 50)
{
    typedef ocl::List<type, size_type> list_type;

    type arg = "a";
    list_type l;

    l.AddHead(arg);
    RESET_TIME();
    CHECK_TIME(l.AddHead(arg));
}

TEST_CONST_MEMBER_FUNCTION_TIME(std_list, push_front, NA, 0, 50)
{
    typedef std::list<type> list_type;

    type arg = "a";
    list_type l;

    RESET_TIME();
    CHECK_TIME(l.push_front(arg));
}

TEST_CONST_MEMBER_FUNCTION_TIME(List, AddTail, NA, 0, 50)
{
    typedef ocl::List<type, size_type> list_type;

    type arg = "a";
    list_type l;

    RESET_TIME();
    CHECK_TIME(l.AddTail(arg));
}

TEST_CONST_MEMBER_FUNCTION_TIME(std_list, push_back, NA, 0, 50)
{
    typedef std::list<type> list_type;

    type arg = "a";
    list_type l;

    RESET_TIME();
    CHECK_TIME(l.push_back(arg));
}

TEST_CONST_MEMBER_FUNCTION_TIME(List, FindNode_a, char_const_ptr, 0, 50)
{
    TEST_OVERRIDE_ARGS("char const*");

    typedef ocl::List<type, size_type> list_type;

    list_type* l = GetOclList();
    bool found = false;

    RESET_TIME();
    CHECK_TIME(found = l->FindNode("a") != nullptr);
    assert(::strcmp(l->FindNode("a")->GetValue(), "a") == 0);
    delete l;
}

TEST_CONST_MEMBER_FUNCTION_TIME(StdList, find_a, char_const_ptr, 0, 50)
{
    TEST_OVERRIDE_ARGS("char const*");

    typedef std::list<type> list_type;

    list_type* l = GetStdList();
    bool found = false;

    RESET_TIME();
    CHECK_TIME(found = std::find(l->begin(), l->end(), "a") != l->end());
    delete l;
}

TEST_CONST_MEMBER_FUNCTION_TIME(List, FindNode_z, char_const_ptr, 0, 50)
{
    TEST_OVERRIDE_ARGS("char const*");

    typedef ocl::List<type, size_type> list_type;

    list_type* l = GetOclList();
    type value = l->GetTailNode()->GetValue();
    bool found = false;

    RESET_TIME();
    CHECK_TIME(found = l->FindNode(value) != nullptr);
    delete l;
}

TEST_CONST_MEMBER_FUNCTION_TIME(StdList, find_z, char_const_ptr, 0, 50)
{
    TEST_OVERRIDE_ARGS("char const*");

    typedef std::list<type> list_type;

    list_type* l = GetStdList();
    auto last = l->end();
    last--;
    type value = *last;
    bool found = false;

    RESET_TIME();
    CHECK_TIME(found = std::find(l->begin(), l->end(), value) != l->end());
    delete l;
}

TEST_CONST_MEMBER_FUNCTION_TIME(List, FindNode, int, 0, 50)
{
    typedef ocl::List<int, size_type> list_type;

    list_type* l = new list_type();
    for (int i = 0; i < 50; ++i)
        l->AddTail(i);

    bool found = false;

    RESET_TIME();
    CHECK_TIME(found = l->FindNode(25) != nullptr);
    delete l;
}

TEST_CONST_MEMBER_FUNCTION_TIME(StdList, find, int, 0, 50)
{
    typedef std::list<int> list_type;

    list_type* l = new list_type();
    for (int i = 0; i < 50; ++i)
        l->push_back(i);

    bool found = false;

    RESET_TIME();
    CHECK_TIME(found = std::find(l->begin(), l->end(), 25) != l->end());
    delete l;
}

TEST_CONST_MEMBER_FUNCTION_TIME(List, InsertBefore, char_const_ptr, 0, 50)
{
    TEST_OVERRIDE_ARGS("char const*");

    typedef ocl::List<type, size_type> list_type;
    typedef list_type::node_type node_type;

    list_type* l = GetOclList();
    node_type* node = l->FindNode("a");

    RESET_TIME();
    CHECK_TIME(l->InsertBefore(node, "11"));

    node_type* prev_node = node->GetPrev();

    while (true)
    {
        if (::strcmp(prev_node->GetValue(), "a") == 0)
            break;
        assert(::strcmp(prev_node->GetValue(), "11") == 0);
        prev_node = prev_node->GetNext();
    }
    delete l;
}

TEST_CONST_MEMBER_FUNCTION_TIME(StdList, insert, char_const_ptr, 0, 50)
{
    TEST_OVERRIDE_ARGS("char const*");

    typedef std::list<type> list_type;

    list_type* l = GetStdList();
    list_type::iterator pos = std::find(l->begin(), l->end(), "a");

    RESET_TIME();
    CHECK_TIME(l->insert(pos, "11"));

    list_type::iterator prev_pos = pos;
    prev_pos--;

    while (true)
    {
        if (::strcmp(*prev_pos, "a") == 0)
            break;
        assert(::strcmp(*prev_pos, "11") == 0);
        ++prev_pos;
    }
    delete l;
}

TEST_CONST_MEMBER_FUNCTION_TIME(List, InsertBefore, int, 0, 50)
{
    typedef ocl::List<int, size_type> list_type;
    typedef list_type::node_type node_type;

    list_type* l = new list_type();
    for (int i = 0; i < 50; ++i)
        l->AddTail(i);

    node_type* node = l->FindNode(25);

    RESET_TIME();
    CHECK_TIME(l->InsertBefore(node, 100));

    node_type* prev_node = node->GetPrev();

    while (true)
    {
        if (prev_node->GetValue() == 25)
            break;
        assert(prev_node->GetValue() == 100);
        prev_node = prev_node->GetNext();
    }
    delete l;
}

TEST_CONST_MEMBER_FUNCTION_TIME(StdList, insert, int, 0, 50)
{
    typedef std::list<int> list_type;

    list_type* l = new list_type();
    for (int i = 0; i < 50; ++i)
        l->push_back(i);

    list_type::iterator pos = std::find(l->begin(), l->end(), 25);

    RESET_TIME();
    CHECK_TIME(l->insert(pos, 100));

    list_type::iterator prev_pos = pos;
    prev_pos--;

    while (true)
    {
        if (*prev_pos == 25)
            break;
        assert(*prev_pos == 100);
        ++prev_pos;
    }
    delete l;
}

TEST_CONST_MEMBER_FUNCTION_TIME(List, Remove, int, 0, 20)
{
    typedef ocl::List<int, size_type> list_type;
    typedef list_type::node_type node_type;

    list_type* l = new list_type();
    for (int i = 0; i < remove_count; ++i)
        l->AddTail(i);

    RESET_TIME();
    CHECK_TIME(node_type* head = l->GetHeadNode(); if (!head) break; l->Remove(head));
    assert(l->GetCount() > 0U);
    delete l;
}

TEST_CONST_MEMBER_FUNCTION_TIME(StdList, erase, int, 0, 20)
{
    typedef std::list<int> list_type;

    list_type* l = new list_type();
    for (int i = 0; i < remove_count; ++i)
        l->push_back(i);

    RESET_TIME();
    CHECK_TIME({ auto it = l->begin(); if (it == l->end()) break; l->erase(it); });
    assert(l->size() > 0U);
    delete l;
}
