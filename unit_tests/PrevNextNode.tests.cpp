/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../PrevNextNode.hpp"
#include <list>
#include <cassert>

namespace
{

struct LargeData
{
    long a;
    long b;
    long c;
    long d;

    LargeData(long a_, long b_, long c_, long d_)
        : a(a_), b(b_), c(c_), d(d_)
    {
    }

    LargeData(LargeData const&) = default;
    LargeData(LargeData&&) = default;
    LargeData& operator =(LargeData const&) = default;
    LargeData& operator =(LargeData&&) = default;
    ~LargeData() {}

    bool operator ==(LargeData const& other) const noexcept
    {
        return a == other.a && b == other.b && c == other.c && d == other.d;
    }
};

}

TEST_MEMBER_FUNCTION(PrevNextNode, constructor, char_const_ptr)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("PrevNextNode", "char const*");

    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type arg = "a";
    node_type node(arg);
    CHECK_TRUE(StrCmp(node.GetValue(), arg) == 0);
}

TEST_MEMBER_FUNCTION(PrevNextNode, constructor, char_const_ptr_move)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("PrevNextNode", "char const*&&");

    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    // Test that the pointer is moved from arg to node.
    type arg = "a";
    type arg_addr = arg;
    node_type node(static_cast<type&&>(arg));
    CHECK_TRUE(arg == nullptr);
    CHECK_TRUE(StrCmp(node.GetValue(), "a") == 0);
    CHECK_TRUE(node.GetValue() == arg_addr);
}

TEST_MEMBER_FUNCTION(PrevNextNode, GetValue, NA)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type value = "a";
    node_type node(value);
    CHECK_TRUE(StrCmp(value, node.GetValue()) == 0);
}

TEST_MEMBER_FUNCTION(PrevNextNode, SetValue, char_const_ptr)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    TEST_OVERRIDE_ARGS("char const*&");

    type value = "a";
    node_type node(value);

    type value2 = "b";
    node.SetValue(value2);
    CHECK_TRUE(StrCmp(node.GetValue(), value2) == 0);
    CHECK_TRUE(node.GetValue() == value2);

    type value3 = "c";
    node.SetValue(static_cast<type&&>(value3));
    CHECK_TRUE(StrCmp(node.GetValue(), "c") == 0);
    CHECK_TRUE(value3 == nullptr);
}

TEST_MEMBER_FUNCTION(PrevNextNode, MoveValue, char_const_ptr)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    TEST_OVERRIDE_ARGS("char const*&");

    type value = "a";
    node_type node(value);

    type value2 = "b";
    node.MoveValue(value2);
    CHECK_TRUE(StrCmp(node.GetValue(), "b") == 0);
    CHECK_TRUE(value2 == nullptr);
}

TEST_MEMBER_FUNCTION(PrevNextNode, GetCount, NA)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    CHECK_EQUAL(nodes[0].GetCount(), 1U);
    nodes[0].AddTail(&nodes[1]);
    CHECK_EQUAL(nodes[0].GetCount(), 2U);
    CHECK_EQUAL(nodes[1].GetCount(), 2U);

    nodes[0].AddHead(&nodes[2]);
    CHECK_EQUAL(nodes[0].GetCount(), 3U);
    CHECK_EQUAL(nodes[1].GetCount(), 3U);
    CHECK_EQUAL(nodes[2].GetCount(), 3U);
}

TEST_MEMBER_FUNCTION(PrevNextNode, GetReverseCount, NA)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    CHECK_EQUAL(nodes[0].GetReverseCount(), 1U);
    nodes[0].AddTail(&nodes[1]);
    CHECK_EQUAL(nodes[0].GetReverseCount(), 1U);
    CHECK_EQUAL(nodes[1].GetReverseCount(), 2U);

    nodes[0].AddHead(&nodes[2]);
    CHECK_EQUAL(nodes[0].GetReverseCount(), 2U);
    CHECK_EQUAL(nodes[1].GetReverseCount(), 3U);
    CHECK_EQUAL(nodes[2].GetReverseCount(), 1U);
}

TEST_MEMBER_FUNCTION(PrevNextNode, GetForwardCount, NA)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    CHECK_EQUAL(nodes[0].GetForwardCount(), 1U);
    nodes[0].AddTail(&nodes[1]);
    CHECK_EQUAL(nodes[0].GetForwardCount(), 2U);
    CHECK_EQUAL(nodes[1].GetForwardCount(), 1U);

    nodes[0].AddHead(&nodes[2]);
    CHECK_EQUAL(nodes[0].GetForwardCount(), 2U);
    CHECK_EQUAL(nodes[1].GetForwardCount(), 1U);
    CHECK_EQUAL(nodes[2].GetForwardCount(), 3U);
}

TEST_MEMBER_FUNCTION(PrevNextNode, SupportHead, NA)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    CHECK_FALSE(node_type::SupportHead());
}

TEST_MEMBER_FUNCTION(PrevNextNode, SupportTail, NA)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    CHECK_FALSE(node_type::SupportTail());
}

TEST_MEMBER_FUNCTION(PrevNextNode, SupportPrevious, NA)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    CHECK_TRUE(node_type::SupportPrevious());
}

TEST_MEMBER_FUNCTION(PrevNextNode, SupportNext, NA)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    CHECK_TRUE(node_type::SupportNext());
}

TEST_MEMBER_FUNCTION(PrevNextNode, GetPrev, NA)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    CHECK_TRUE(nodes[0].GetPrev() == nullptr);

    nodes[0].AddHead(&nodes[1]);
    CHECK_TRUE(nodes[0].GetPrev() == &nodes[1]);

    // Test that adding a new head has inserted the prev_node2 in front of prev_node.
    nodes[0].AddHead(&nodes[2]);
    CHECK_TRUE(nodes[1].GetPrev() == &nodes[2]);
}

TEST_MEMBER_FUNCTION(PrevNextNode, GetNext, NA)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    CHECK_TRUE(nodes[0].GetNext() == nullptr);

    // Test that adding a new tail has inserted the next_node after node.
    nodes[0].AddTail(&nodes[1]);
    CHECK_TRUE(nodes[0].GetNext() == &nodes[1]);

    // Test that adding a new tail has inserted the next_node2 after next_node.
    nodes[0].AddTail(&nodes[2]);
    CHECK_TRUE(nodes[1].GetNext() == &nodes[2]);
}

TEST_MEMBER_FUNCTION(PrevNextNode, GetHead, NA)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    CHECK_TRUE(nodes[0].GetHead() == &nodes[0]);

    nodes[0].AddTail(&nodes[1]);
    CHECK_TRUE(nodes[1].GetHead() == &nodes[0]);

    nodes[0].AddHead(&nodes[2]);
    CHECK_TRUE(nodes[0].GetHead() == &nodes[2]);
    CHECK_TRUE(nodes[2].GetNext() == &nodes[0]);
}

TEST_MEMBER_FUNCTION(PrevNextNode, GetTail, NA)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    CHECK_TRUE(nodes[0].GetHead() == &nodes[0]);

    nodes[0].AddTail(&nodes[1]);
    CHECK_TRUE(nodes[0].GetTail() == &nodes[1]);

    nodes[0].AddHead(&nodes[2]);
    CHECK_TRUE(nodes[0].GetHead() == &nodes[2]);
    CHECK_TRUE(nodes[0].GetTail() == &nodes[1]);
}

TEST_MEMBER_FUNCTION(PrevNextNode, FindValue, NA)
{
    {
        typedef char const* type;
        typedef ocl::PrevNextNode<type> node_type;

        type args[] = { "a", "b", "c" };
        node_type nodes[] = { args[0], args[1], args[2] };

        nodes[0].AddTail(&nodes[1]);
        nodes[0].AddTail(&nodes[2]);

        CHECK_TRUE(nodes[0].FindValue("a") == &nodes[0]);
        CHECK_TRUE(nodes[1].FindValue("a") == &nodes[0]);
        CHECK_TRUE(nodes[2].FindValue("a") == &nodes[0]);

        CHECK_TRUE(nodes[0].FindValue("b") == &nodes[1]);
        CHECK_TRUE(nodes[1].FindValue("b") == &nodes[1]);
        CHECK_TRUE(nodes[2].FindValue("b") == &nodes[1]);

        CHECK_TRUE(nodes[0].FindValue("c") == &nodes[2]);
        CHECK_TRUE(nodes[1].FindValue("c") == &nodes[2]);
        CHECK_TRUE(nodes[2].FindValue("c") == &nodes[2]);

        CHECK_TRUE(nodes[0].FindValue("d") == nullptr);
    }

    {
        typedef wchar_t const* type;
        typedef ocl::PrevNextNode<type> node_type;

        type args[] = { L"a", L"b", L"c" };
        node_type nodes[] = { args[0], args[1], args[2] };

        nodes[0].AddTail(&nodes[1]);
        nodes[0].AddTail(&nodes[2]);

        CHECK_TRUE(nodes[0].FindValue(L"a") == &nodes[0]);
        CHECK_TRUE(nodes[1].FindValue(L"a") == &nodes[0]);
        CHECK_TRUE(nodes[2].FindValue(L"a") == &nodes[0]);

        CHECK_TRUE(nodes[0].FindValue(L"b") == &nodes[1]);
        CHECK_TRUE(nodes[1].FindValue(L"b") == &nodes[1]);
        CHECK_TRUE(nodes[2].FindValue(L"b") == &nodes[1]);

        CHECK_TRUE(nodes[0].FindValue(L"c") == &nodes[2]);
        CHECK_TRUE(nodes[1].FindValue(L"c") == &nodes[2]);
        CHECK_TRUE(nodes[2].FindValue(L"c") == &nodes[2]);

        CHECK_TRUE(nodes[0].FindValue(L"d") == nullptr);
    }

    {
        typedef char16_t const* type;
        typedef ocl::PrevNextNode<type> node_type;

        type args[] = { u"a", u"b", u"c" };
        node_type nodes[] = { args[0], args[1], args[2] };

        nodes[0].AddTail(&nodes[1]);
        nodes[0].AddTail(&nodes[2]);

        CHECK_TRUE(nodes[0].FindValue(u"a") == &nodes[0]);
        CHECK_TRUE(nodes[1].FindValue(u"a") == &nodes[0]);
        CHECK_TRUE(nodes[2].FindValue(u"a") == &nodes[0]);

        CHECK_TRUE(nodes[0].FindValue(u"b") == &nodes[1]);
        CHECK_TRUE(nodes[1].FindValue(u"b") == &nodes[1]);
        CHECK_TRUE(nodes[2].FindValue(u"b") == &nodes[1]);

        CHECK_TRUE(nodes[0].FindValue(u"c") == &nodes[2]);
        CHECK_TRUE(nodes[1].FindValue(u"c") == &nodes[2]);
        CHECK_TRUE(nodes[2].FindValue(u"c") == &nodes[2]);

        CHECK_TRUE(nodes[0].FindValue(u"d") == nullptr);
    }

    {
        typedef char32_t const* type;
        typedef ocl::PrevNextNode<type> node_type;

        type args[] = { U"a", U"b", U"c" };
        node_type nodes[] = { args[0], args[1], args[2] };

        nodes[0].AddTail(&nodes[1]);
        nodes[0].AddTail(&nodes[2]);

        CHECK_TRUE(nodes[0].FindValue(U"a") == &nodes[0]);
        CHECK_TRUE(nodes[1].FindValue(U"a") == &nodes[0]);
        CHECK_TRUE(nodes[2].FindValue(U"a") == &nodes[0]);

        CHECK_TRUE(nodes[0].FindValue(U"b") == &nodes[1]);
        CHECK_TRUE(nodes[1].FindValue(U"b") == &nodes[1]);
        CHECK_TRUE(nodes[2].FindValue(U"b") == &nodes[1]);

        CHECK_TRUE(nodes[0].FindValue(U"c") == &nodes[2]);
        CHECK_TRUE(nodes[1].FindValue(U"c") == &nodes[2]);
        CHECK_TRUE(nodes[2].FindValue(U"c") == &nodes[2]);

        CHECK_TRUE(nodes[0].FindValue(U"d") == nullptr);
    }
}

TEST_MEMBER_FUNCTION(PrevNextNode, FindPrevValue, NA)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    nodes[0].AddTail(&nodes[1]);
    nodes[0].AddTail(&nodes[2]);

    CHECK_TRUE(nodes[0].FindPrevValue("a") == &nodes[0]);
    CHECK_TRUE(nodes[1].FindPrevValue("a") == &nodes[0]);
    CHECK_TRUE(nodes[2].FindPrevValue("a") == &nodes[0]);

    CHECK_TRUE(nodes[0].FindPrevValue("b") == nullptr);
    CHECK_TRUE(nodes[1].FindPrevValue("b") == &nodes[1]);
    CHECK_TRUE(nodes[2].FindPrevValue("b") == &nodes[1]);

    CHECK_TRUE(nodes[0].FindPrevValue("c") == nullptr);
    CHECK_TRUE(nodes[1].FindPrevValue("c") == nullptr);
    CHECK_TRUE(nodes[2].FindPrevValue("c") == &nodes[2]);

    CHECK_TRUE(nodes[0].FindValue("d") == nullptr);
}

TEST_MEMBER_FUNCTION(PrevNextNode, FindNextValue, NA)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    nodes[0].AddTail(&nodes[1]);
    nodes[0].AddTail(&nodes[2]);

    CHECK_TRUE(nodes[0].FindNextValue("a") == &nodes[0]);
    CHECK_TRUE(nodes[1].FindNextValue("a") == nullptr);
    CHECK_TRUE(nodes[2].FindNextValue("a") == nullptr);

    CHECK_TRUE(nodes[0].FindNextValue("b") == &nodes[1]);
    CHECK_TRUE(nodes[1].FindNextValue("b") == &nodes[1]);
    CHECK_TRUE(nodes[2].FindNextValue("b") == nullptr);

    CHECK_TRUE(nodes[0].FindNextValue("c") == &nodes[2]);
    CHECK_TRUE(nodes[1].FindNextValue("c") == &nodes[2]);
    CHECK_TRUE(nodes[2].FindNextValue("c") == &nodes[2]);

    CHECK_TRUE(nodes[0].FindValue("d") == nullptr);
}

TEST_MEMBER_FUNCTION(PrevNextNode, AddHead, node_ptr)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    CHECK_TRUE(nodes[0].GetHead() == &nodes[0]);

    nodes[0].AddHead(&nodes[1]);
    CHECK_TRUE(nodes[0].GetHead() == &nodes[1]);
    CHECK_TRUE(nodes[0].GetPrev() == &nodes[1]);

    nodes[0].AddHead(&nodes[2]);
    CHECK_TRUE(nodes[0].GetHead() == &nodes[2]);
    CHECK_TRUE(nodes[1].GetHead() == &nodes[2]);
    CHECK_TRUE(nodes[1].GetPrev() == &nodes[2]);
}

TEST_MEMBER_FUNCTION(PrevNextNode, AddTail, node_ptr)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    CHECK_TRUE(nodes[0].GetTail() == &nodes[0]);

    nodes[0].AddTail(&nodes[1]);
    CHECK_TRUE(nodes[0].GetNext() == &nodes[1]);
    CHECK_TRUE(nodes[0].GetTail() == &nodes[1]);

    nodes[0].AddTail(&nodes[2]);
    CHECK_TRUE(nodes[0].GetTail() == &nodes[2]);
    CHECK_TRUE(nodes[1].GetNext() == &nodes[2]);
}

TEST_MEMBER_FUNCTION(PrevNextNode, InsertBefore, node_ptr)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    nodes[0].InsertBefore(&nodes[1]);
    CHECK_TRUE(nodes[0].GetHead() == &nodes[1]);

    nodes[0].InsertBefore(&nodes[2]);
    CHECK_TRUE(nodes[0].GetHead() == &nodes[1]);
    CHECK_TRUE(nodes[0].GetPrev() == &nodes[2]);
}

TEST_MEMBER_FUNCTION(PrevNextNode, InsertAfter, node_ptr)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    nodes[0].InsertAfter(&nodes[1]);
    CHECK_TRUE(nodes[0].GetTail() == &nodes[1]);

    nodes[0].InsertAfter(&nodes[2]);
    CHECK_TRUE(nodes[0].GetTail() == &nodes[1]);
    CHECK_TRUE(nodes[0].GetNext() == &nodes[2]);
}

TEST_MEMBER_FUNCTION(PrevNextNode, Remove, NA)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    nodes[0].AddTail(&nodes[1]);
    nodes[0].AddTail(&nodes[2]);
    nodes[1].Remove();
    CHECK_TRUE(nodes[0].GetNext() == &nodes[2]);
    CHECK_TRUE(nodes[2].GetPrev() == &nodes[0]);
    CHECK_TRUE(nodes[1].GetPrev() == nullptr);
    CHECK_TRUE(nodes[1].GetNext() == nullptr);
}

TEST_MEMBER_FUNCTION(PrevNextNode, Swap, NA)
{
    {
        typedef char const* type;
        typedef ocl::PrevNextNode<type> node_type;

        type args[] = { "a", "b", "c" };
        node_type nodes[] = { args[0], args[1], args[2] };

        nodes[0].AddTail(&nodes[1]);
        nodes[0].AddTail(&nodes[2]);
        nodes[0].Swap(&nodes[1]);
        node_type* head = nodes[0].GetHead();
        node_type* next = head->GetNext();
        CHECK_EQUAL(StrCmp(head->GetValue(), "b"), 0);
        CHECK_EQUAL(StrCmp(next->GetValue(), "a"), 0);
    }

    {
        typedef LargeData type;
        typedef ocl::PrevNextNode<type> node_type;

        type args[] = { LargeData(1, 2, 3, 4), LargeData(5, 6, 7, 8), LargeData(9, 10, 11, 12) };
        node_type nodes[] = { args[0], args[1], args[2] };

        nodes[0].AddTail(&nodes[1]);
        nodes[0].AddTail(&nodes[2]);
        nodes[0].Swap(&nodes[1]);
        node_type* head = nodes[0].GetHead();
        node_type* next = head->GetNext();
        CHECK_TRUE(head->GetValue() == LargeData(5, 6, 7, 8));
        CHECK_TRUE(next->GetValue() == LargeData(1, 2, 3, 4));
    }
}

TEST_MEMBER_FUNCTION(PrevNextNode, UnsafeInsertBetween, node_ptr_node_ptr_node_ptr)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    TEST_OVERRIDE_ARGS("node*, node*, node*");

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    nodes[0].InsertAfter(&nodes[1]);
    CHECK_TRUE(nodes[0].GetTail() == &nodes[1]);

    node_type::UnsafeInsertBetween(&nodes[0], &nodes[1], &nodes[2]);
    CHECK_TRUE(nodes[0].GetNext() == &nodes[2]);
    CHECK_TRUE(nodes[1].GetPrev() == &nodes[2]);
}

TEST_MEMBER_FUNCTION(PrevNextNode, UnsafeAddHead, node_ptr_node_ptr)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    TEST_OVERRIDE_ARGS("node*, node*");

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    node_type::UnsafeAddHead(&nodes[0], &nodes[1]);
    CHECK_TRUE(nodes[0].GetHead() == &nodes[1]);
    CHECK_TRUE(nodes[1].GetNext() == &nodes[0]);

    node_type::UnsafeAddHead(&nodes[0], &nodes[2]);
    CHECK_TRUE(nodes[0].GetHead() == &nodes[2]);
    CHECK_TRUE(nodes[2].GetNext() == &nodes[0]);
}

TEST_MEMBER_FUNCTION(PrevNextNode, UnsafeAddTail, node_ptr_node_ptr)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    TEST_OVERRIDE_ARGS("node*, node*");

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    node_type::UnsafeAddTail(&nodes[0], &nodes[1]);
    CHECK_EQUAL(nodes[0].GetCount(), 2U);
    CHECK_TRUE(nodes[0].GetTail() == &nodes[1]);
    CHECK_TRUE(nodes[1].GetPrev() == &nodes[0]);

    node_type::UnsafeAddTail(&nodes[1], &nodes[2]);
    CHECK_EQUAL(nodes[0].GetCount(), 3U);
    CHECK_TRUE(nodes[0].GetTail() == &nodes[2]);
    CHECK_TRUE(nodes[2].GetPrev() == &nodes[1]);
}

TEST_MEMBER_FUNCTION(PrevNextNode, UnsafeRemove, node_ptr_node_ptr)
{
    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    TEST_OVERRIDE_ARGS("node*, node*");

    type args[] = { "a", "b", "c" };
    node_type nodes[] = { args[0], args[1], args[2] };

    nodes[0].UnsafeAddTail(&nodes[1]);
    nodes[0].UnsafeAddTail(&nodes[2]);

    node_type::UnsafeRemove(&nodes[1]);
    CHECK_EQUAL(nodes[0].GetCount(), 2U);
    CHECK_NULL(nodes[0].FindValue(args[1]));

    node_type::UnsafeRemove(&nodes[2]);
    CHECK_EQUAL(nodes[0].GetCount(), 1U);
    CHECK_NULL(nodes[0].FindValue(args[2]));
}
