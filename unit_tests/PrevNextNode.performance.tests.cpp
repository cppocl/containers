/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../PrevNextNode.hpp"
#include <list>
#include <cassert>

namespace
{

    typedef char const* type;

    type args[53] =
    {
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", ""
    };

    template<typename NodeType>
    void PrevNextNodePushFront(NodeType*& head_node, type value, size_t& count)
    {
        typedef ocl::PrevNextNode<type> node_type;

        node_type* node_to_add = node_type::Allocate(value);
        head_node->UnsafeAddHead(node_to_add);
        head_node = node_to_add;
        ++count;
    }

    template<typename ListType>
    void ListPushFront(ListType& l, type value, size_t& count)
    {
        l.push_front(value);
        ++count;
    }

}

TEST_CONST_MEMBER_FUNCTION_TIME(PrevNextNode, AddHead, char_const_ptr, 0, 50)
{
    TEST_OVERRIDE_ARGS("char const*");

    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    type arg = "a";
    node_type* head_node = node_type::Allocate(arg);

    size_t count = 0;
    RESET_TIME();
    CHECK_TIME(PrevNextNodePushFront(head_node, arg, count));

    node_type::FreeNodes(head_node);
}

TEST_CONST_MEMBER_FUNCTION_TIME(list, push_front, char_const_ptr, 0, 50)
{
    TEST_OVERRIDE_ARGS("char const*");

    typedef std::list<type> list_type;

    type arg = "a";
    list_type l;
    l.push_front(arg);

    size_t count = 0;
    RESET_TIME();
    CHECK_TIME(ListPushFront(l, arg, count));
}

TEST_CONST_MEMBER_FUNCTION_TIME(PrevNextNode, FindValue_a, char_const_ptr, 0, 50)
{
    TEST_OVERRIDE_ARGS("char const*");

    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    node_type* head_node = node_type::Allocate(args[0]);
    node_type* last_node = head_node;
    for( size_t pos = 1; args[pos][0] != '\0'; ++pos)
    {
        node_type* node = node_type::Allocate(args[pos]);
        last_node->AddTail(node);
        last_node = node;
    }

    assert(::strcmp(last_node->GetValue(), "z") == 0);
    bool found;
    RESET_TIME();
    CHECK_TIME(found = head_node->FindValue("a") != nullptr);
    assert(found);

    node_type::FreeNodes(head_node);
}

TEST_CONST_MEMBER_FUNCTION_TIME(PrevNextNode, FindNextValue_a, char_const_ptr, 0, 50)
{
    TEST_OVERRIDE_ARGS("char const*");

    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    node_type* head_node = node_type::Allocate(args[0]);
    node_type* last_node = head_node;
    for (size_t pos = 1; args[pos][0] != '\0'; ++pos)
    {
        node_type* node = node_type::Allocate(args[pos]);
        last_node->AddTail(node);
        last_node = node;
    }

    assert(::strcmp(last_node->GetValue(), "z") == 0);
    bool found;
    RESET_TIME();
    CHECK_TIME(found = head_node->FindNextValue("a") != nullptr);
    assert(found);

    node_type::FreeNodes(head_node);
}

TEST_CONST_MEMBER_FUNCTION_TIME(PrevNextNode, FindPrevValue_a, char_const_ptr, 0, 50)
{
    TEST_OVERRIDE_ARGS("char const*");

    typedef char const* type;
    typedef ocl::PrevNextNode<type> node_type;

    node_type* head_node = node_type::Allocate(args[0]);
    node_type* last_node = head_node;
    for (size_t pos = 1; args[pos][0] != '\0'; ++pos)
    {
        node_type* node = node_type::Allocate(args[pos]);
        last_node->AddTail(node);
        last_node = node;
    }

    assert(::strcmp(last_node->GetValue(), "z") == 0);
    bool found;
    RESET_TIME();
    CHECK_TIME(found = last_node->FindPrevValue("a") != nullptr);
    assert(found);

    node_type::FreeNodes(head_node);
}
