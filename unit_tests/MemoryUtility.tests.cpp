/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../MemoryUtility.hpp"
#include <cstddef>

namespace
{

struct TestData
{
    TestData()
        : m_char_value('a')
        , m_int_value(2)
    {
    }

    char m_char_value;
    int  m_int_value;
};

}

TEST_MEMBER_FUNCTION(MemoryUtility, Allocate, NA)
{
    {
        typedef char type;
        typedef std::size_t size_type;

        bool exception = false;

        typedef ocl::MemoryUtility<type, size_type> memory_utility_type;

        CHECK_TRUE(memory_utility_type::is_primitive);

        type* ptr = nullptr;
        try
        {
            ptr = memory_utility_type::Allocate();
            CHECK_NOT_NULL(ptr);
            memory_utility_type::Free(ptr);
        }

        catch (...)
        {
            exception = true;
        }

        CHECK_FALSE(exception);
    }

    {
        typedef TestData type;
        typedef std::size_t size_type;

        bool exception = false;

        typedef ocl::MemoryUtility<type, size_type> memory_utility_type;

        CHECK_FALSE(memory_utility_type::is_primitive);

        type* ptr = nullptr;
        try
        {
            ptr = memory_utility_type::Allocate();
            CHECK_NOT_NULL(ptr);
            CHECK_EQUAL(ptr->m_char_value, 'a');
            CHECK_EQUAL(ptr->m_int_value, 2);
            memory_utility_type::Free(ptr);
        }

        catch (...)
        {
            exception = true;
        }

        CHECK_FALSE(exception);
    }
}

TEST_MEMBER_FUNCTION(MemoryUtility, Allocate, type)
{
    {
        typedef char type;
        typedef std::size_t size_type;

        bool exception = false;

        typedef ocl::MemoryUtility<type, size_type> memory_utility_type;

        type* ptr = nullptr;
        try
        {
            ptr = memory_utility_type::Allocate('a');
            CHECK_NOT_NULL(ptr);
            CHECK_EQUAL(*ptr, 'a');
            memory_utility_type::Free(ptr);
        }

        catch (...)
        {
            exception = true;
        }

        CHECK_FALSE(exception);
    }

    {
        typedef TestData type;
        typedef std::size_t size_type;

        bool exception = false;

        typedef ocl::MemoryUtility<type, size_type> memory_utility_type;

        type* ptr = nullptr;
        try
        {
            TestData init;
            init.m_char_value = 'c';
            init.m_int_value = 4;

            ptr = memory_utility_type::Allocate(init);
            CHECK_NOT_NULL(ptr);
            CHECK_EQUAL(ptr->m_char_value, 'c');
            CHECK_EQUAL(ptr->m_int_value, 4);
            memory_utility_type::Free(ptr);
        }

        catch (...)
        {
            exception = true;
        }

        CHECK_FALSE(exception);
    }
}

TEST_MEMBER_FUNCTION(MemoryUtility, AllocateArray, size_t)
{
    {
        typedef char type;
        typedef std::size_t size_type;

        bool exception = false;

        typedef ocl::MemoryUtility<type, size_type> memory_utility_type;

        type* ptr = nullptr;
        try
        {
            ptr = memory_utility_type::AllocateArray(2);
            CHECK_NOT_NULL(ptr);
            memory_utility_type::FreeArray(ptr);
        }

        catch (...)
        {
            exception = true;
        }

        CHECK_FALSE(exception);
    }
}
