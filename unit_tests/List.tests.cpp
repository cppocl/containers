/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../List.hpp"
#include <list>

TEST_MEMBER_FUNCTION(List, constructor, NA)
{
    TEST_OVERRIDE_FUNCTION_NAME("List");

    typedef char const* type;
    typedef ocl::List<type> list_type;

    list_type l;
    CHECK_EQUAL(l.GetCount(), 0U);
}

TEST_MEMBER_FUNCTION(List, constructor, char_const_ptr)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("List", "char const*");

    typedef char const* type;
    typedef ocl::List<type> list_type;

    list_type l;
    CHECK_EQUAL(l.GetCount(), 0U);
}

TEST_MEMBER_FUNCTION(List, GetCount, NA)
{
    typedef char const* type;
    typedef ocl::List<type> list_type;

    list_type l;
    CHECK_EQUAL(l.GetCount(), 0U);

    l.AddTail("a");
    CHECK_EQUAL(l.GetCount(), 1U);

    l.AddHead("b");
    CHECK_EQUAL(l.GetCount(), 2U);

    l.AddHead("c");
    CHECK_EQUAL(l.GetCount(), 3U);

    l.AddTail("d");
    CHECK_EQUAL(l.GetCount(), 4U);
}

TEST_MEMBER_FUNCTION(List, GetHeadNode, NA)
{
    typedef char const* type;
    typedef ocl::List<type> list_type;

    list_type l;
    CHECK_NULL(l.GetHeadNode());

    l.AddTail("a");
    CHECK_NOT_NULL(l.GetHeadNode());
    CHECK_EQUAL(StrCmp(l.GetHeadNode()->GetValue(), "a"), 0);

    l.AddHead("b");
    CHECK_NOT_NULL(l.GetHeadNode());
    CHECK_EQUAL(StrCmp(l.GetHeadNode()->GetValue(), "b"), 0);
    CHECK_NOT_NULL(l.GetTailNode());
    CHECK_EQUAL(StrCmp(l.GetTailNode()->GetValue(), "a"), 0);
}

TEST_MEMBER_FUNCTION(List, GetTailNode, NA)
{
    typedef char const* type;
    typedef ocl::List<type> list_type;

    list_type l;
    CHECK_NULL(l.GetTailNode());

    l.AddTail("a");
    CHECK_NOT_NULL(l.GetTailNode());
    CHECK_EQUAL(StrCmp(l.GetTailNode()->GetValue(), "a"), 0);

    l.AddTail("b");
    CHECK_NOT_NULL(l.GetTailNode());
    CHECK_EQUAL(StrCmp(l.GetTailNode()->GetValue(), "b"), 0);
    CHECK_NOT_NULL(l.GetHeadNode());
    CHECK_EQUAL(StrCmp(l.GetHeadNode()->GetValue(), "a"), 0);
}

TEST_MEMBER_FUNCTION(List, AddHead, NA)
{
    typedef char const* type;
    typedef ocl::List<type> list_type;

    list_type l;

    l.AddHead("a");
    CHECK_NOT_NULL(l.GetHeadNode());
    CHECK_EQUAL(StrCmp(l.GetHeadNode()->GetValue(), "a"), 0);

    l.AddHead("b");
    CHECK_NOT_NULL(l.GetHeadNode());
    CHECK_EQUAL(StrCmp(l.GetHeadNode()->GetValue(), "b"), 0);
    CHECK_NOT_NULL(l.GetHeadNode()->GetNext());
    CHECK_EQUAL(StrCmp(l.GetHeadNode()->GetNext()->GetValue(), "a"), 0);
}

TEST_MEMBER_FUNCTION(List, AddTail, NA)
{
    typedef char const* type;
    typedef ocl::List<type> list_type;

    list_type l;

    l.AddTail("a");
    CHECK_NOT_NULL(l.GetHeadNode());
    CHECK_EQUAL(StrCmp(l.GetHeadNode()->GetValue(), "a"), 0);

    l.AddTail("b");
    CHECK_NOT_NULL(l.GetTailNode());
    CHECK_EQUAL(StrCmp(l.GetTailNode()->GetValue(), "b"), 0);
    CHECK_EQUAL(StrCmp(l.GetHeadNode()->GetValue(), "a"), 0);
}

TEST_MEMBER_FUNCTION(List, FindNode, NA)
{
    typedef char const* type;
    typedef ocl::List<type> list_type;

    list_type l;

    l.AddTail("a");
    CHECK_NOT_NULL(l.FindNode("a"));
    CHECK_EQUAL(StrCmp(l.FindNode("a")->GetValue(), "a"), 0);
    CHECK_NULL(l.FindNode("b"));

    l.AddTail("b");
    CHECK_NOT_NULL(l.FindNode("a"));
    CHECK_NOT_NULL(l.FindNode("b"));
    CHECK_EQUAL(StrCmp(l.FindNode("b")->GetValue(), "b"), 0);
    CHECK_NULL(l.FindNode("c"));
}

TEST_MEMBER_FUNCTION(List, Exists, NA)
{
    typedef char const* type;
    typedef ocl::List<type> list_type;

    list_type l;

    l.AddTail("a");
    CHECK_TRUE(l.Exists("a"));
    CHECK_FALSE(l.Exists("b"));

    l.AddTail("b");
    CHECK_TRUE(l.Exists("a"));
    CHECK_TRUE(l.Exists("b"));
}
